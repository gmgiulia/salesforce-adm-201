If you are going to take ADM201, then you should have a ggod understanding of the below concepts:

1. Organization Setup: 3%
2. User Setup: 7%
3. Security and Access: 13%
4. Standard and Custom Objects: 14%
5. Sales and Marketing Applications: 14%
6. Service and Support Applications: 13%
7. Activity Management and Collaboration: 3%
8. Data Management: 10%
9. Analytics—Reports and Dashboards: 10%
10. Workflow/Process Automation: 8%
11. Desktop and Mobile Administration: 3%
12. AppExchange: 2%

The ADM201 exam contains 60 multiple choice questions.
Time allocated for the exam is 90 minutes. 
To pass you should get at least 65% which means  39 correctly answered questions.
Registration fee: USD 200 plus applicable taxes as required per local law.
Retake fee: USD 100 plus applicable taxes as required per local law.
Delivery options: Proctored exam delivered onsite at a testing center or in an online proctored environment.



In this repository you can find material and link to help you study and
pass your Salesforce Adm-201 Exam and become a Certified Salesforce Administrator.

For more material and info have a look at my blog:

mysalesforcedeveloperjourney.wordpress.com/salesforce-system-administrator


